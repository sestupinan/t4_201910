package model.util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.util.Sort;
import model.vo.VOMovingViolation;

public class SortTest {

	// Muestra de datos a ordenar
	private Comparable[] datos;
	
	private Comparable[] ordenada;
	
	@Before
	public void setupScenario1()
	{
		datos[1] = new VOMovingViolation(13234919,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-01-03T13:29:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[2] = new VOMovingViolation(13237800,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-02-03T12:39:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[3] = new VOMovingViolation(13237804,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-03-03T15:17:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[4] = new VOMovingViolation(13237830,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-01-03T19:58:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[5] = new VOMovingViolation(13237849,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-02-03T17:36:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[6] = new VOMovingViolation(13237884,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-03-03T15:07:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[7] = new VOMovingViolation(13237918,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-01-03T16:15:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[8] = new VOMovingViolation(13238939,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-02-01T13:45:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[9] = new VOMovingViolation(14466883,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-02-03T14:59:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[10] =new VOMovingViolation(14466905,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-03-01T15:36:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
	}
	
	/**
	 * Arreglo ordenado con el que se comparar� el ordenamiendo de los datos del setupScenario1
	 */
	@Before
	public void setupScenarioOrdenado()
	{
		datos[1] = new VOMovingViolation(13234919,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-01-03T13:29:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");		
		datos[2] = new VOMovingViolation(13237918,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-01-03T16:15:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[3] = new VOMovingViolation(13237830,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-01-03T19:58:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		
		datos[4] = new VOMovingViolation(13237800,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-02-03T12:39:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[5] = new VOMovingViolation(13238939,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-02-01T13:45:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[6] = new VOMovingViolation(14466883,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-02-03T14:59:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[7] = new VOMovingViolation(13237849,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-02-03T17:36:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		
		datos[8] = new VOMovingViolation(13237884,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-03-03T15:07:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[9] = new VOMovingViolation(13237804,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-03-03T15:17:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
		datos[10] =new VOMovingViolation(14466905,"DC 295 .4 MI S/O PENN AVE SE SW/B","","","401550.69,133804.8","Moving","100",100,0,0,"No","2018-03-01T15:36:00.000Z","T119","SPEED 11-15 MPH OVER THE SPEED LIMIT,");
	}
	
	/**
     * <b>Prueba:</b> Verifica el m�todo ordenarShellSort.<br>
     * <b>M�todos a probar:</b><br>
     * OrdenarShellSort<br>
     * Size<br>
     * <b>Casos de prueba:</b><br>
     * Ordena los datos exitosamnete.
     * El primer dato del arreglo coincide con el dato ordenado.
     * El ultimo dato del arreglo coincide con el ultimo dato del arreglo ordenado
     * El tama�p del arreglo sigue siendo el mismo, orden� exitosamente
     */
    @SuppressWarnings("deprecation")
	@Test
	public void ordenarShellSortTest()
	{
		Sort.ordenarShellSort(datos);
		int x = datos.length;
		assertEquals("Ese no deber�a ser el primer dato, ordenamiento incorrecto", datos[1], ordenada[1] );
		assertEquals("Ese no deber�a ser el ultimo dato, ordenamiento incorrecto", datos[10], ordenada[10] );
		assertEquals("ordenamiento incorrecto", datos, ordenada );
		assertEquals("elimin� algun elemento de la lista o insert� uno nuevo, mala ejecuci�n", x, datos.length);
	}
	
    /**
     * <b>Prueba:</b> Verifica el m�todo ordenarMergeSort.<br>
     * <b>M�todos a probar:</b><br>
     * OrdenarMergeSort<br>
     * Size<br>
     * <b>Casos de prueba:</b><br>
     * Ordena los datos exitosamnete.
     * El primer dato del arreglo coincide con el dato ordenado.
     * El ultimo dato del arreglo coincide con el ultimo dato del arreglo ordenado
     * El tama�o del arreglo sigue siendo el mismo, orden� exitosamente
     */
	public void ordenarMergeSortTest()
	{
		Sort.ordenarMergeSort(datos,0 , datos.length-1);
		int x = datos.length;
		assertEquals("Ese no deber�a ser el primer dato, ordenamiento incorrecto", datos[1], ordenada [1]);
		assertEquals("Ese no deber�a ser el dato en esa posicion, ordenamiento incorrecto", datos[5], ordenada [5]);
		assertEquals("Ese no deber�a ser el ultimo dato, ordenamiento incorrecto", datos[10], ordenada[10]);
		assertEquals("elimin� algun elemento de la lista o insert� uno nuevo, mala ejecuci�n", x, datos.length);
	}
	
	/**
     * <b>Prueba:</b> Verifica el m�todo ordenarQuickSort.<br>
     * <b>M�todos a probar:</b><br>
     * OrdenarQuickSort<br>
     * Size<br>
     * <b>Casos de prueba:</b><br>
     * Ordena los datos exitosamnete.
     * El primer dato del arreglo coincide con el dato ordenado.
     * El ultimo dato del arreglo coincide con el ultimo dato del arreglo ordenado
     * El tama�o del arreglo sigue siendo el mismo, orden� exitosamente
     */
	public void ordenarQuickSortTest()
	{
		Sort.ordenarQuickSort(datos, 0, datos.length-1);
		int x = datos.length;
		assertEquals("Ese no deber�a ser el primer dato, ordenamiento incorrecto", datos[1], ordenada[1]);
		assertEquals("Ese no deber�a ser el dato en esa posicion, ordenamiento incorrecto", datos[5], ordenada [5]);
		assertEquals("Ese no deber�a ser el ultimo dato, ordenamiento incorrecto", datos[10], ordenada[10] );
		assertEquals("elimin� algun elemento de la lista o insert� uno nuevo, mala ejecuci�n", x, datos.length);
	}
	
	@Before
	public void setUp() throws Exception{
		System.out.println("Codigo de configuracion de muestra de datos a probar");
	}

	@Test
	public void test() {
		Sort.ordenarMergeSort(datos, 0 ,1);
		fail("Not yet implemented");
	}

}
