package model.util;

public class Sort {

	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarShellSort( Comparable[ ] datos ) {
		
		// TODO implementar el algoritmo ShellSort
		int N = datos.length;
		int h = 1;
		while (h < N/3)
		h = 3*h + 1;
		while (h >= 1) {
			for (int i = h; i < N; i++) {
					for (int j = i; j >= h && less(datos[j], datos[j-h]); j -= h)
						exchange(datos, j, j-h);
		}
		h = h/3;
		}

		
	}
	
	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarMergeSort( Comparable[ ] datos, int left, int right ) 
	{

		if (left < right) 
	    { 
	        // large l and h 
	        int m = left+(right-left)/2; 
	  
	        // Sort first and second halves 
	        ordenarMergeSort(datos, left, m); 
	        ordenarMergeSort(datos, m+1, right); 
	  
	        merge(datos, left, m, right); 
	    } 	}

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarQuickSort( Comparable[ ] datos, int left, int right ) 
	{
		if (right <= left) return;
		int j = partition(datos, left, right);
		ordenarQuickSort(datos, left, j-1); // Sort left part a[lo .. j-1].
		ordenarQuickSort(datos, j+1, right); // Sort right part a[j+1 .. hi].
		
	}
	
	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable v, Comparable w)
	{
		boolean rta = false;
		if(v.compareTo(w)<0)
		{
			rta = true;
		}		
		return rta;
	}
	
	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		Comparable aux = datos[i];
		datos[i] = datos[j];
		datos[j] = aux;
	}
	
	
	public static void merge(Comparable datos[], int left, int medium, int right) 
    { 
        // Find sizes of two subarrays to be merged
        int n1 = medium - left + 1; 
        int n2 = right - medium; 
  
        /* Create temp arrays */
        Comparable L[] = new Comparable [n1]; 
        Comparable R[] = new Comparable [n2]; 
  
        /*Copy data to temp arrays*/
        for (int i=0; i<n1; ++i) 
            L[i] = datos[left + i]; 
        for (int j=0; j<n2; ++j) 
            R[j] = datos[medium + 1+ j]; 
  
  
        /* Merge the temp arrays */
  
        // Initial indexes of first and second subarrays 
        int i = 0, j = 0; 
  
        // Initial index of merged subarry array 
        int k = left; 
        while (i < n1 && j < n2) 
        { 
            if (L[i].compareTo(R[j]) <= 0) 
            { 
                datos[k] = L[i]; 
                i++; 
            } 
            else
            { 
                datos[k] = R[j]; 
                j++; 
            } 
            k++; 
        } 
  
        /* Copy remaining elements of L[] if any */
        while (i < n1) 
        { 
            datos[k] = L[i]; 
            i++; 
            k++; 
        } 
  
        /* Copy remaining elements of R[] if any */
        while (j < n2) 
        { 
            datos[k] = R[j]; 
            j++; 
            k++; 
        } 
    } 
	
	public static int partition(Comparable arr[], int low, int high) 
    { 
		int i = low, j = high+1; // left and right scan indices
		Comparable v = arr[low]; // partitioning item
		while (true)
		{ // Scan right, scan left, check for scan complete, and exchange.
		while (less(arr[++i], v)) if (i == high) break;
		while (less(v, arr[--j])) if (j == low) break;
		if (i >= j) break;
		exchange(arr, i, j);
		}
		exchange(arr, low, j); // Put v = a[j] into position
		return j; // with a[lo..j-1] <= a[j] <= a[j+1..hi].

    } 
}
