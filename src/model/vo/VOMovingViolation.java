package model.vo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolation implements Comparable<VOMovingViolation> {


	
	private int id;
	private String location;
	private String address;
	private String streetId;
	private String xCoordinates;
	private String yCoordinates;
	private String ticketType;
	private int fine;
	private int totalPaid;
	private int penalty1;
	private String accidentIndicator;
	private String ticketIssuedDate;
	private String violationCode;
	private String violationDescription;
	
	public VOMovingViolation(int pId, String pLocation, String pAddress, String pStreetId, String pXCoordinates, String pYCoordinates, String pTicketType, int pFine, int pTotalPaid, int pPenalty, String pAccidentIndicator, String pTicketIssuedDate, String pViolationCode, String pViolationDescription)
	{
		id = pId;
		location = pLocation;
		address = pAddress;
		streetId = pStreetId;
		xCoordinates = pXCoordinates;
		yCoordinates = pYCoordinates;
		ticketType = pTicketType;
		fine = pFine;
		totalPaid = pTotalPaid;
		penalty1 = pPenalty;
		accidentIndicator = pAccidentIndicator;
		ticketIssuedDate = pTicketIssuedDate;
		violationCode = pViolationCode;
		violationDescription = pViolationDescription;
	}
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getStreetId() {
		return streetId;
	}


	public void setStreetId(String streetId) {
		this.streetId = streetId;
	}


	public String getxCoordinates() {
		return xCoordinates;
	}


	public void setxCoordinates(String xCoordinates) {
		this.xCoordinates = xCoordinates;
	}


	public String getyCoordinates() {
		return yCoordinates;
	}


	public void setyCoordinates(String yCoordinates) {
		this.yCoordinates = yCoordinates;
	}


	public String getTicketType() {
		return ticketType;
	}


	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}


	public int getFine() {
		return fine;
	}


	public void setFine(int fine) {
		this.fine = fine;
	}


	public int getPenalty1() {
		return penalty1;
	}


	public void setPenalty1(int penalty1) {
		this.penalty1 = penalty1;
	}


	public String getTicketIssuedDate() {
		return ticketIssuedDate;
	}


	public void setTicketIssuedDate(String ticketIssuedDate) {
		this.ticketIssuedDate = ticketIssuedDate;
	}


	public String getViolationCode() {
		return violationCode;
	}


	public void setViolationCode(String violationCode) {
		this.violationCode = violationCode;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getTotalPaid() {
		return totalPaid;
	}

	public void setTotalPaid(int totalPaid) {
		this.totalPaid = totalPaid;
	}

	public String getAccidentIndicator() {
		return accidentIndicator;
	}

	public void setAccidentIndicator(String accidentIndicator) {
		this.accidentIndicator = accidentIndicator;
	}

	public String getViolationDescription() {
		return violationDescription;
	}

	public void setViolationDescription(String violationDescription) {
		this.violationDescription = violationDescription;
	}
	
	@Override
	public String toString() 
	{
		return "Issue Date:" + this.ticketIssuedDate + "  ID: " + this.id;
	}

	@Override
	public int compareTo(VOMovingViolation arg0) {
		
		DateFormat issueDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		int comp = 0;
		Date date1 = new Date();
		Date date2 = new Date();
		try 
		{
			String fecha1 = this.getTicketIssuedDate();
			String fecha2 = arg0.getTicketIssuedDate();
			date1 = issueDate.parse(fecha1);
			date2 = issueDate.parse(fecha2);
			comp = date1.compareTo(date2);
			
		} 
		catch (ParseException e) {
			e.printStackTrace();
		}
		if(comp < 0)
		{
			return comp;
		}
		else if(comp > 0)
		{
			return comp;
		}
		else
		{
			int id1 = this.getId();
			int id2 = arg0.getId();
			if(id1 > id2)
			{
				return 1;
			}
			else if(id1 < id2)
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}
		
		
	}



}
