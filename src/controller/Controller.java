package controller;

import java.io.FileReader;
import java.util.Random;
import java.util.Scanner;

import com.opencsv.CSVReader;

import model.*;
import model.data_structures.*;
import model.util.Sort;
import model.vo.VOMovingViolation;
import model.vo.*;
import view.MovingViolationsManagerView;

@SuppressWarnings("unused")
public class Controller {

	public static final String file1 = "./data/Moving_Violations_Issued_in_January_2018.csv";
	public static final String file2 = "./data/Moving_Violations_Issued_in_February_2018.csv";
	public static final String file3 = "./data/Moving_Violations_Issued_in_March_2018.csv";
	public static final String file4 = "./data/Moving_Violations_Issued_in_April_2018.csv";
	public static final String file5 = "./data/Moving_Violations_Issued_in_May_2018.csv";
	public static final String file6 = "./data/Moving_Violations_Issued_in_June_2018.csv";
	public static final String file7 = "./data/Moving_Violations_Issued_in_July_2018.csv";
	public static final String file8 = "./data/Moving_Violations_Issued_in_August_2018.csv";
	public static final String file9 = "./data/Moving_Violations_Issued_in_September_2018.csv";
	public static final String file10 = "./data/Moving_Violations_Issued_in_October_2018.csv";
	public static final String file11 = "./data/Moving_Violations_Issued_in_Novemeber_2018.csv";
	public static final String file12 = "./data/Moving_Violations_Issued_in_December_2018.csv";
	
	private MovingViolationsManagerView view;
	
	// TODO Definir las estructuras de datos para cargar las infracciones del periodo definido
	private DLL <VOMovingViolation> movingViolationsDLL = new DLL<VOMovingViolation>();
	// Muestra obtenida de los datos cargados 
	Comparable<VOMovingViolation> [ ] muestra;

	// Copia de la muestra de datos a ordenar 
	Comparable<VOMovingViolation> [ ] muestraCopia;

	public Controller() {
		view = new MovingViolationsManagerView();
		movingViolationsDLL= new DLL<VOMovingViolation>();
		
		//TODO inicializar las estructuras de datos para la carga de informacion de archivos
	}

	/**
	 * Leer los datos de las infracciones de los archivos. Cada infraccion debe ser Comparable para ser usada en los ordenamientos.
	 * Todas infracciones (MovingViolation) deben almacenarse en una Estructura de Datos (en el mismo orden como estan los archivos)
	 * A partir de estos datos se obtendran muestras para evaluar los algoritmos de ordenamiento
	 * @return numero de infracciones leidas 
	 */
	public int loadMovingViolations(String archivo) {
		// TODO Los datos de los archivos deben guardarse en la Estructura de Datos definida
		CSVReader reader;
		int count = 0;
		try 
		{
			
			reader = new CSVReader(new FileReader(archivo));
			String [] nextLine = new String[16];
			nextLine = reader.readNext();
			while ((nextLine = reader.readNext()) != null) 
			{
				VOMovingViolation nuevo = new VOMovingViolation(Integer.parseInt(nextLine[0]), nextLine[2], nextLine[3], nextLine[4], nextLine[5], nextLine[6], nextLine[7], Integer.parseInt(nextLine[8]), Integer.parseInt(nextLine[9]), Integer.parseInt(nextLine[10]), nextLine[12], nextLine[13], nextLine[14], nextLine[15]);
				System.out.println("" + nuevo.getId());
				movingViolationsDLL.push(nuevo);
				count++;
			}
			
			reader.close();
		} 
		catch (Exception e) 
		{
			e  = new Exception("Hubo un error cargando los archivos");
		}
		return count;
	}
	
	/**
	 * Generar una muestra aleatoria de tamaNo n de los datos leidos.
	 * Los datos de la muestra se obtienen de las infracciones guardadas en la Estructura de Datos.
	 * @param n tamaNo de la muestra, n > 0
	 * @return muestra generada
	 * @throws Exception 
	 */
	public Comparable<VOMovingViolation> [ ] generarMuestra( int n ) 
	{

		muestra = new Comparable[ n ];
		int dondeAgregar = 0;
		Comparable<Integer> randoms[] = new Integer[n];
		Random rand = new Random();
		while(n>dondeAgregar)
		{
			int random = rand.nextInt(240000)+1;
			randoms[dondeAgregar] = random;
			dondeAgregar++;
		}
		dondeAgregar = 0;
		int counter = 0;
		Sort.ordenarMergeSort(randoms, 0, randoms.length-1);
		DLLNode<VOMovingViolation> actual = movingViolationsDLL.getHead();
		for (int i = 0; i < randoms.length; i++) 
		{
			if(randoms[i].compareTo(counter) == 0)
			{
				muestra[dondeAgregar] = actual.getData();
				dondeAgregar++;
				actual = movingViolationsDLL.getHead();
			}
			else
			{
				actual = actual.getNext();
				counter++;
				i--;
			}
		}
		int index = 0;
		Comparable<VOMovingViolation> temp = null;
	    Random random = new Random();
	    for (int i = muestra.length - 1; i > 0; i--)
	    {
	        index = random.nextInt(i + 1);
	        temp = muestra[index];
	        muestra[index] = muestra[i];
	        muestra[i] = temp;
	    }
		return muestra;

	}
	
	/**
	 * Generar una copia de una muestra. Se genera un nuevo arreglo con los mismos elementos.
	 * @param muestra - datos de la muestra original
	 * @return copia de la muestra
	 */
	public Comparable<VOMovingViolation> [ ] obtenerCopia( Comparable<VOMovingViolation> [ ] muestra)
	{
		Comparable<VOMovingViolation> [ ] copia = new Comparable[ muestra.length ]; 
		for ( int i = 0; i < muestra.length; i++)
		{    copia[i] = muestra[i];    }
		return copia;
	}
	
	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public void ordenarShellSort( Comparable<VOMovingViolation>[ ] datos ) {
		
		Sort.ordenarShellSort(datos);
	}
	
	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public void ordenarMergeSort( Comparable<VOMovingViolation>[ ] datos ) {

		Sort.ordenarMergeSort(datos, 0, datos.length-1);
	}

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public void ordenarQuickSort( Comparable<VOMovingViolation>[ ] datos ) {

		Sort.ordenarQuickSort(datos, 0, datos.length-1);
	}

	/**
	 * Invertir una muestra de datos (in place).
	 * datos[0] y datos[N-1] se intercambian, datos[1] y datos[N-2] se intercambian, datos[2] y datos[N-3] se intercambian, ...
	 * @param datos - conjunto de datos a invertir (inicio) y conjunto de datos invertidos (final)
	 */
	public void invertirMuestra( Comparable[ ] datos ) 
	{
		int i = 0;
		int j = datos.length-1;
		while( i<j)
		{
			Comparable aux = datos[i];
			datos[i] = datos[j];
			datos[j] = aux;
			j--;
			i++;		
		}
	}
	
	public void run() {
		long startTime;
		long endTime;
		long duration;
		
		int nDatos = 0;
		int nMuestra = 0;
		
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		
		while(!fin)
		{
			view.printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					// Cargar infracciones
					nDatos += this.loadMovingViolations(file1);
					nDatos += this.loadMovingViolations(file2);
					nDatos += this.loadMovingViolations(file3);

					view.printMensage("Numero infracciones cargadas:" + nDatos);
					break;
					
				case 2:
					// Generar muestra de infracciones a ordenar
					view.printMensage("Dar tamaNo de la muestra: ");
					nMuestra = sc.nextInt();
					if(nMuestra <= 240000)
					{
						muestra = this.generarMuestra( nMuestra );
						view.printMensage("Muestra generada");
					}
					else
					{
						view.printMensage("Error: TamaNo de muestra no debe superar 240000");
					}
					break;
					
				case 3:
					// Mostrar los datos de la muestra actual (original)
					if ( nMuestra > 0 && muestra != null && muestra.length == nMuestra )
					{    
						view.printDatosMuestra( nMuestra, muestra);
					}
					else
					{
						view.printMensage("Muestra invalida");
					}
					break;

				case 4:
					// Aplicar ShellSort a una copia de la muestra
					if ( nMuestra > 0 && muestra != null && muestra.length == nMuestra )
					{
						muestraCopia = this.obtenerCopia(muestra);
						startTime = System.currentTimeMillis();
						this.ordenarShellSort(muestraCopia);
						endTime = System.currentTimeMillis();
						duration = endTime - startTime;
						view.printMensage("Ordenamiento generado en una copia de la muestra");
						view.printMensage("Tiempo de ordenamiento ShellSort: " + duration + " milisegundos");
					}
					else
					{
						view.printMensage("Muestra invalida");
					}
					break;
					
				case 5:
					// Aplicar MergeSort a una copia de la muestra
					if ( nMuestra > 0 && muestra != null && muestra.length == nMuestra )
					{
						muestraCopia = this.obtenerCopia(muestra);
						startTime = System.currentTimeMillis();
						this.ordenarMergeSort(muestraCopia);
						endTime = System.currentTimeMillis();
						duration = endTime - startTime;
						view.printMensage("Ordenamiento generado en una copia de la muestra");
						view.printMensage("Tiempo de ordenamiento MergeSort: " + duration + " milisegundos");
					}
					else
					{
						view.printMensage("Muestra invalida");
					}
					break;
											
				case 6:
					// Aplicar QuickSort a una copia de la muestra
					if ( nMuestra > 0 && muestra != null && muestra.length == nMuestra )
					{
						muestraCopia = this.obtenerCopia(muestra);
						startTime = System.currentTimeMillis();
						this.ordenarQuickSort(muestraCopia);
						endTime = System.currentTimeMillis();
						duration = endTime - startTime;
						view.printMensage("Ordenamiento generado en una copia de la muestra");
						view.printMensage("Tiempo de ordenamiento QuickSort: " + duration + " milisegundos");
					}
					else
					{
						view.printMensage("Muestra invalida");
					}
					break;
											
				case 7:
					// Mostrar los datos de la muestra ordenada (muestra copia)
					if ( nMuestra > 0 && muestraCopia != null && muestraCopia.length == nMuestra )
					{    view.printDatosMuestra( nMuestra, muestraCopia);    }
					else
					{
						view.printMensage("Muestra Ordenada invalida");
					}
					break;
					
				case 8:	
					// Una muestra ordenada se convierte en la muestra a ordenar
					if ( nMuestra > 0 && muestraCopia != null && muestraCopia.length == nMuestra )
					{    
						muestra = muestraCopia;
						view.printMensage("La muestra ordenada (copia) es ahora la muestra de datos a ordenar");
					}
					break;

				case 9:
					// Invertir la muestra a ordenar
					if ( nMuestra > 0 && muestra != null && muestra.length == nMuestra )
					{    
						this.invertirMuestra(muestra);
						view.printMensage("La muestra de datos a ordenar fue invertida");
					}
					else
					{
						view.printMensage("Muestra invalida");
					}

					break;
					
				case 10:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

}
